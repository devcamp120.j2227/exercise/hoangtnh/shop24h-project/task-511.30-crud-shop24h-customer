//Import thư viện mongoose
const mongoose = require("mongoose");

//tạo class schema lấy từ thư viện mongoose
const Schema = mongoose.Schema;

//Khởi tạo productSchema từ class Schema
const productSchema = new Schema ({
    name: {
        type: String,
        unique: true,
        required: true,
    },
    description: {
        type: String
    },
    type: {
        type: mongoose.Types.ObjectId,
        ref: "ProductType",
        required: true,
    },
    imageUrl: {
        type: String,
        required: true
    },
    buyPrice: {
        type: Number,
        required: true
    },
    promotionPrice: {
        type: Number,
        required: true
    },
    amount: {
        type: Number,
        default: 0
    }
});

//export product type model từ productSchema
module.exports = mongoose.model("Product", productSchema);