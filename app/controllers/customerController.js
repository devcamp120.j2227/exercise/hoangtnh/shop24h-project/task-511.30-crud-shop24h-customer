//Khai báo thư viện mongoose
const { response } = require("express");
const mongoose = require("mongoose");

//Import customer model 
const customerModel = require("../model/customerModel");


//function create customer
const createCustomer = (request, response) => {
    //B1: chuẩn bị dữ liệu
    const body = request.body;
    //B2: validate dữ liệu
    if(!body.fullName) {
        return response.status(400).json({
            status:"Bad Request",
            message: "Full name không hợp lệ"
        })
    }
    if(!body.phone){
        return response.status(400).json({
            status:"Bad Request",
            message: "Số điện thoại không hợp lệ"
        })
    }
    if(!body.email){
        return response.status(400).json({
            status:"Bad Request",
            message: "Email không hợp lệ"
        })
    }
    //B3: gọi model tạo dữ liệu customer mới
    const newCustomer = {
        _id: mongoose.Types.ObjectId(),
        fullName: body.fullName,
        phone: body.phone,
        email: body.email,
        address: body.address,
        city: body.city,
        country: body.country,
        order: body.order
    }

    customerModel.create(newCustomer, (error, data) => {
        if(error) {
            return response.status(500).json({
                status: "Internal server error",
                message: error.message
            })
        }
        return response.status(201).json({
            status:"Created new customer successfully",
            data: data
        })
    })
}

//function get all customer
const getAllCustomer = (request, response) => {
    customerModel.find((error, data) =>{
        if(error){
            return response.status(500).json({
                status: "Internal server error",
                message: error.message
            })
        }
        return response.status(201).json({
            status:"get all customers successfully",
            data: data
        })
    })
}

//function get customer by id
const getCustomerById = (request, response) => {
    //B1: chuẩn bị dữ liệu
    const customerId = request.params.customerId;
    //B2: validate id có tồn tại hay không?
    if(!mongoose.Types.ObjectId.isValid(customerId)){
        return response.status(400).json({
            status:"Bad request",
            message: "customer id không hợp lệ"
        })
    }
    //B3: gọi model customer tìm dữ liệu bằng id
    customerModel.findById(customerId, (error, data) => {
        if(error){
            return response.status(500).json({
                status: "Internal server error",
                message: error.message
            })
        }
        return response.status(200).json({
            status:"Get customer detail successfully",
            data: data
        })
    })
}

//function update customer by id
const updateCustomerById = (request, response) => {
    //B1: chuẩn bị dữ liệu gồm id và body json
    const customerId = request.params.customerId;
    const body = request.body;

    //B2: validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(customerId)){
        return response.status(400).json({
            status:"Bad request",
            message: "customer id không hợp lệ"
        })
    }
    if(!body.fullName) {
        return response.status(400).json({
            status:"Bad Request",
            message: "Full name không hợp lệ"
        })
    }
    if(!body.phone){
        return response.status(400).json({
            status:"Bad Request",
            message: "Số điện thoại không hợp lệ"
        })
    }
    if(!body.email){
        return response.status(400).json({
            status:"Bad Request",
            message: "Email không hợp lệ"
        })
    }
    //B3: gọi model update dữ liệu customer
    const updateCustomer = {};
    if(body.fullName !== undefined){
        updateCustomer.fullName = body.fullName
    }
    if(body.phone !==  undefined){
        updateCustomer.phone = body.phone
    }
    if(body.email !==  undefined){
        updateCustomer.email = body.email
    }
    if(body.address !==  undefined){
        updateCustomer.address = body.address
    }
    if(body.city !==  undefined){
        updateCustomer.city = body.city
    }
    if(body.country !==  undefined){
        updateCustomer.country = body.country
    }
    customerModel.findByIdAndUpdate(customerId, updateCustomer, (error, data) =>{
        if(error){
            return response.status(500).json({
                status:"Internal server error",
                message: error.message
            })
        }
        return response.status(200).json({
            status:"Update customer successfully",
            data: data
        })
    })
}
//function delete customer by id
const deleteCustomerById = (request, response) =>{
    //B1: chuẩn bị dữ liệu
    const customerId = request.params.customerId;

    //B2: validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(customerId)){
        return response.status(400).json({
            status: "Bad request",
            message: "customer id không hợp lệ"
        })
    }
    //B3: gọi model customer bằng id cần xóa
    customerModel.findByIdAndRemove(customerId, (error, data) => {
        if(error){
            return response.status(500).json({
                status:"Internal server error",
                mesage: error.mesage
            })
        }
        return response.status(200).json({
            status: "Deleted customer successfully"
        })
    })
}
module.exports = {
    createCustomer,
    getAllCustomer,
    getCustomerById,
    updateCustomerById,
    deleteCustomerById
}